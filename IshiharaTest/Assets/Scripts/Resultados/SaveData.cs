using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveData : MonoBehaviour
{
    [SerializeField] private TestData _testData = new TestData();

    public void SaveIntoJson()
    {
        string data = JsonUtility.ToJson(_testData);
        System.IO.File.WriteAllText(Application.persistentDataPath + "/TestData.json", data);
    }
}

[System.Serializable]
public class TestData
{
    public string[] TestResults;
    public int value;
    public List<Results> result = new List<Results>();
}

[System.Serializable]
public class Results
{
    public string option;
    public string optionResult;
}