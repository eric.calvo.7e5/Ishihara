using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DataWork : MonoBehaviour
{
    public PreguntasBehaviour preguntasBehaviour;
    private int[] correctVision = {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,3,3,3,3,1,1,1,1,1,1,3,3,1,1,1,1,1,1,1,1,1};
    private int[] daltonicVision = {1,2,2,2,2,2,2,2,2,3,3,3,3,3,3,3,3,2,2,2,2,2,2,2,2,2,2,2,2,3,3,3,3,2,2,2,2,1};
    float porcentajeCorrectVision;
    float porcentajeDaltonicVision;
    
    
    public void SceneChange()
    {
        SceneManager.LoadScene("Results");
    }
    
    public void Calculate()
    {
        int correctAnswers = 0;
        for (int i = 0; i < preguntasBehaviour.preguntas.Length-1; i++)
        {
            if (preguntasBehaviour.preguntas[i].resposta == correctVision[i])
            {
                correctAnswers++;
            }
        }
        //Un poco flipao lo de calcular porcentajes as� pero es funcional xd
        porcentajeCorrectVision = (correctAnswers / 38f) * 100f;
        porcentajeDaltonicVision = 100f - porcentajeCorrectVision;
        Debug.Log(porcentajeCorrectVision);
        Invoke("PrintResult",0.5f);
    }

    public void PrintResult()
    {
        GameObject.Find("ResultsText").GetComponent<Text>().text = "El percentatge de daltonisme, �s de: " + porcentajeDaltonicVision + "%" + "\n" +
                                                                   "El percentatge de visi� normal, �s de: " + porcentajeCorrectVision + "%";
    }

    // Start is called before the first frame update
    private void Start()
    {
        DontDestroyOnLoad(gameObject);
        
    }

    // Update is called once per frame
    private void Update()
    {
    }
}