using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PreguntasBehaviour : MonoBehaviour
{
    public GameObject previousButton;
    public GameObject nextButton;
    public GameObject image;
    public GameObject sendButton;
    public GameObject dropdown;
    public GameObject label;
    public Text title;
    private int contador = 0;

    public string[][] opcionesDropdown = {  new string[] {" ", "12","20","Nada"},
                                            new string[] {" ", "8","3","Nada"},
                                            new string[] {" ", "6","5","Nada"},
                                            new string[] {" ", "29","70","Nada"},
                                            new string[] {" ", "57","35","Nada"},
                                            new string[] {" ", "5","2","Nada"},
                                            new string[] {" ", "3","5","Nada"},
                                            new string[] {" ", "15","17","Nada"},
                                            new string[] {" ", "74","21","Nada"},
                                            new string[] {" ", "2","4","Nada"},
                                            new string[] {" ", "6","9","Nada"},
                                            new string[] {" ", "97","12","Nada"},
                                            new string[] {" ", "45","80","Nada"},
                                            new string[] {" ", "5","1","Nada"},
                                            new string[] {" ", "7","10","Nada"},
                                            new string[] {" ", "16","7","Nada"},
                                            new string[] {" ", "73","42","Nada"},
                                            new string[] {" ", "10","5","Nada"},
                                            new string[] {" ", "17","2","Nada"},
                                            new string[] {" ", "20","45","Nada"},
                                            new string[] {" ", "12","73","Nada"},
                                            new string[] {" ", "26","6", "2","Nada"},
                                            new string[] {" ", "42","2", "4", "Nada"},
                                            new string[] {" ", "35","5", "3","Nada"},
                                            new string[] {" ", "96","6","9", "Nada"},
                                            new string[] {" ", "L�nia de punts morats i vermells","L�nia morada","L�nia vermella","Nada"},
                                            new string[] {" ", "L�nia de punts vermells i morats","L�nia morada","L�nia vermella","Nada"},
                                            new string[] {" ", "Un quadrat","Una l�nia","Nada"},
                                            new string[] {" ", "Un cercle", "Una l�nia", "Nada"},
                                            new string[] {" ", "L�nia blava-verda", "Un rectangle","Nada"},
                                            new string[] {" ", "L�nia blava-verda", "Un triangle","Nada"},
                                            new string[] {" ", "L�nia taronja","L�nia verda","Nada"},
                                            new string[] {" ", "L�nia taronja", "L�nia verda","Nada"},
                                            new string[] {" ", "L�nia verda", "L�nia vermella  i una altre violeta", "Nada"},
                                            new string[] {" ", "L�nia verda","L�nia blava i una altre violeta","Nada"},
                                            new string[] {" ", "L�nia taronja","L�nia blava-verda","Nada"},
                                            new string[] {" ", "L�nia taronja","L�nia blava-verda","Nada"},
                                            new string[] {" ", "L�nia taronja","L�nia blava","Nada"}};

    [System.Serializable]
    public class Pregunta
    {
        public Sprite lamina;
        public int resposta;
        //public string[] opcions;
    }

    public Pregunta[] preguntas;

    public void SiguientePregunta()
    {
        preguntas[contador].resposta = dropdown.GetComponent<TMP_Dropdown>().value;
        contador++;
        ActualizarUI();
        dropdown.GetComponent<TMP_Dropdown>().value = preguntas[contador].resposta;
        image.GetComponent<Image>().sprite = preguntas[contador].lamina;
        title.text = "Pregunta " + (contador + 1) + "/38";
    }

    public void AnteriorPregunta()
    {
        preguntas[contador].resposta = dropdown.GetComponent<TMP_Dropdown>().value;
        contador--;
        ActualizarUI();
        dropdown.GetComponent<TMP_Dropdown>().value = preguntas[contador].resposta;
        image.GetComponent<Image>().sprite = preguntas[contador].lamina;
        title.text = "Pregunta " + (contador + 1) + "/38";
    }

    public void ActualizarUI()
    {
        List<TMP_Dropdown.OptionData> currentOptions = dropdown.GetComponent<TMP_Dropdown>().options;
        int desiredOptionsCount = opcionesDropdown[contador].Length;

        // Cambiar el n�mero de opciones de dropdown
        if(desiredOptionsCount <= 4) {
            if(currentOptions.Count > 4) {
                for(int i = 0; i < currentOptions.Count - 4; i++) {
                    currentOptions.RemoveAt(0);
                }
            }
        } else {
            while(currentOptions.Count < desiredOptionsCount) {
                currentOptions.Add(new TMP_Dropdown.OptionData());
            }
        }

        // Asignar valores de respuesta a opciones de dropdown
        for (int i = 0; i < opcionesDropdown[contador].Length; i++)
        {           
            dropdown.GetComponent<TMP_Dropdown>().options[i].text = opcionesDropdown[contador][i];
        }
        
        previousButton.SetActive(contador != 0);
        nextButton.SetActive(contador != preguntas.Length - 1);
        sendButton.SetActive(contador == preguntas.Length - 1);
    }

    private void Start()
    {
        ActualizarUI();
    }
}